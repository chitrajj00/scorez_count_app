const storage = {
  token: "rajuToken",
  userInfo: "rajuInfo",
  userName: "raju",
  userEmail: "raju1@raju1.com",
  password: "my_password",
};


class User {
  setToken = token => {
    localStorage.setItem(storage.token, token);
  };
  getDataKey = key => {
    return localStorage.getItem(storage[key]);
  };
  setUserInfo = info => {
    localStorage.setItem(storage.userInfo, info);
  };
  isLogin() {
    return localStorage.getItem(storage.token) &&
      localStorage.getItem(storage.userInfo)
      ? true
      : false;
  }

  loginAttempt = (useremail, password) => {
    if (storage.userEmail === useremail && storage.password === password) {
      return true;
    }
    return false;
  };


  registerAttempt = (username, email, password, password_confirmation) => {
    if (storage.userName === username && storage.password === password &&
        storage.userEmail === email && storage.password === password_confirmation) {
      return true;
    }
    return false;
  };


  clearData() {
    localStorage.removeItem(storage.token);
    localStorage.removeItem(storage.userInfo);
  }
}
export default new User();
