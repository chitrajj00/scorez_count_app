import { IS_LOGIN, IS_REGISTER, USER_NAME, EMAIL, USER_PASSWORD, USER_PASSWORD_CON } from "../actions/action-type";


const initialState = {
  name: "raju",
  email:"raju1@raju1.com",
  password: "my_password",
  loading: false,
  success: false,
  error: false
};

const userReducers = (state = initialState, action) => {
  switch (action.type) {
    case USER_NAME:
      return {
        ...state,
        name: action.username
      };
    case EMAIL:
      return {
        ...state,
        email: action.email
      };
    case USER_PASSWORD:
      return {
        ...state,
        password: action.password
      };
    case USER_PASSWORD_CON:
      return {
        ...state,
        password_confirmation: action.password_confirmation
      };
    case IS_REGISTER:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        token: action.token
      };
    case IS_LOGIN:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        token: action.token
      };
    default:
      return state;
  }
};

export default userReducers;
