import React, { Component } from "react";
import Login from "./components/Login";
import Register from "./components/Register";
import Dashboard from "./components/Pages";
import Orders from "./components/Pages/Orders";
import Games from "./components/Pages/Games";
import Integrations from "./components/Pages/Integrations";
import Reports from "./components/Pages/Reports";
import { store } from "./helpers";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Wrapper from "./components/Wrapper";

const wrapperDashboard = Wrapper(Dashboard);
const wrapperOrders = Wrapper(Orders);
const wrapperReports = Wrapper(Reports);
const wrapperIntegrations = Wrapper(Integrations);
const wrapperGames = Wrapper(Games);
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/signin" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/dashboard" component={wrapperDashboard} />
            <Route exact path="/games" component={wrapperGames} />
            <Route exact path="/players" component={wrapperOrders} />
            <Route exact path="/reports" component={wrapperReports} />
            <Route
              exact
              path="/integrations"
              component={wrapperIntegrations}
            />
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
