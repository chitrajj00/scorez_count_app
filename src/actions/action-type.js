/**
 * Redux işlem tipleri
 */

export const IS_LOGIN = "IS_LOGIN";
export const IS_REGISTER = "IS_REGISTER";
export const LOADING = "LOADING";
export const USER_NAME = "IS_USER_NAME";
export const EMAIL = "IS_EMAIL";
export const USER_PASSWORD = "IS_USER_PASSWORD";
export const USER_PASSWORD_CON = "IS_USER_PASSWORD_CON";
export const USER_SUCCESS = "USER_SUCCESS";
export const USER_ERROR = "USER_ERROR";
export const ALERT_SUCCESS = "ALERT_SUCCESS";
export const ALERT_ERROR = "ALERT_ERROR";
export const ALERT_CLOSE = "ALERT_CLOSE";

export const DASHBOARD_DRAWER_OPEN = "DASHBOARD_DRAWER_OPEN";
