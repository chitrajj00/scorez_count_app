import { IS_LOGIN, IS_REGISTER, USER_NAME, EMAIL, USER_PASSWORD, USER_PASSWORD_CON } from "./action-type";
import { pageLoading, alertError, alertSuccess } from "./alert-actions";
import User from "../tools/user-service";

export const userNameChange = username => {
  const type = USER_NAME;
  return { type, username };
};

export const userEmailChange = email => {
  const type = EMAIL;
  return { type, email };
};

export const userPasswordChange = password => {
  const type = USER_PASSWORD;
  return { type, password };
};

export const userPasswordConfirmationChange = password_confirmation => {
  const type = USER_PASSWORD_CON;
  return { type, password_confirmation };
};


export const userLogin = (token, username) => {
  const type = IS_LOGIN;
  User.setToken(token);
  User.setUserInfo(username);
  return { type, token };
};


export const userRegister = (token, username) => {
  const type = IS_REGISTER;
  User.setToken(token);
  User.setUserInfo(username);
  return { type, token };
};



export const userClear = () => {
  const type = IS_LOGIN;
  User.clearData();
  return { type, token: null };
};

export const userLoginSubmit = (username, password) => {
  return dispatch => {
    dispatch(pageLoading());
    if (User.loginAttempt(username, password)) {
      dispatch(userLogin(Math.random(), username));
      return dispatch(
        alertSuccess("Login is successful !! Redirecting to ScoresApp...")
      );
    }
    return dispatch(alertError("Username Password Incorrect"));
  };
};


export const userRegisterSubmit = (username, email, password, password_confirmation) => {
  return dispatch => {
    dispatch(pageLoading());
    if (User.registerAttempt(username, email, password, password_confirmation)) {
      dispatch(userRegister(Math.random(), username));
      return dispatch(
        alertSuccess("SignUp is successful !! Redirecting ...")
      );
    }
    return dispatch(alertError("Please check the fields"));
  };
};


export const userLogout = () => {
  return dispatch => {
    /*dispatch(userPassword("1"));*/
    dispatch(alertSuccess("The session was closed successfully !! We wait again ..."));
    dispatch(userClear());
  };
};
