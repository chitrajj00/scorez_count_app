import React, { Component } from "react";
import { User } from "../includes";
import WrapperTemplate from "./PageTemplate/WrapperTemplate";

const Wrapper = WrappedComponent => {
  return class extends Component {
    render() {
      if (User.isLogin()) {
        return (
          <WrapperTemplate {...this.props} c_user={User.getDataKey("userInfo")}>
            <WrappedComponent {...this.props} />
          </WrapperTemplate>
        );
      } else {
        console.log("please login.........");
        this.props.history.push("/");
        return null;
      }
    }
  };
};
export default Wrapper;
