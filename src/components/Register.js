import React from "react";
import PropTypes from "prop-types";
import {
  withStyles,
  CssBaseline,
  Button,
  Paper,
  Avatar,
  LockIcon,
  Typography,
  FormControl,
  User
} from "../includes";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";

import { connect } from "react-redux";
import {
  userNameChange,
  userEmailChange,
  userPasswordChange,
  userPasswordConfirmationChange,
  userRegisterSubmit
} from "../includes/actions";
import Snackbar from "./Snackbar";
const styles = theme => ({
  layout: {
    width: "auto",
    display: "block", // Fix IE11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  }
});

class Register extends React.Component {
  isRegistered() {
    if (User.isLogin()) {
      this.props.history.push("/dashboard");
    }
  }
  componentDidUpdate() {
    this.isRegistered();
  }
  componentWillMount() {
    this.isRegistered();
  }
  onUserSignIn() {
    this.props.history.push("/signin");
  }
  handleChange = name => event => {
    if (name === "username") {
      this.props.onUserNameChange(event.target.value);
    } else if (name === "email") {
      this.props.onUserEmailChange(event.target.value);
    }else if (name === "password") {
      this.props.onUserPasswordChange(event.target.value);
    }else if (name === "password_confirmation") {
      this.props.onUserPasswordConfirmationChange(event.target.value);
    }
  };
  onSubmit(event) {
    event.preventDefault();
    this.props.onRegisterSubmit(
      this.props.user.name,
      this.props.user.email,
      this.props.user.password,
      this.props.user.password
    );
  }

  render() {
    // console.log("register1=====");
    // console.log(this.props);
    // console.log("register1=====end");
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Snackbar {...this.props.alert} />
        <CssBaseline />
        <main className={classes.layout}>
          <Paper className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockIcon />
            </Avatar>
            <Typography variant="headline">Register</Typography>
            <ValidatorForm
              className={classes.form}
              onSubmit={this.onSubmit.bind(this)}
              onError={errors => console.log(errors)}
            >
              <FormControl margin="normal" required fullWidth>
                <TextValidator
                  label="User Name"
                  validators={["required"]}
                  name="username"
                  id="username"
                  value={this.props.user.name}
                  errorMessages={["This field is required"]}
                  onChange={this.handleChange("username")}
                />
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <TextValidator
                  label="Email"
                  validators={["required"]}
                  name="email"
                  id="email"
                  value={this.props.user.email}
                  errorMessages={["This field is required"]}
                  onChange={this.handleChange("email")}
                />
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <TextValidator
                  label="Password"
                  name="password"
                  type="password"
                  validators={["required"]}
                  errorMessages={["This field is required"]}
                  id="password"
                  value={this.props.user.password}
                  autoComplete="current-password"
                  onChange={this.handleChange("password")}
                />
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <TextValidator
                  label="Password Confirmation"
                  name="password_confirmation"
                  type="password"
                  validators={["required"]}
                  errorMessages={["This field is required"]}
                  id="password_confirmation"
                  value={this.props.user.password}
                  autoComplete="current-password"
                  onChange={this.handleChange("password_confirmation")}
                />
              </FormControl>

              <Button
                fullWidth
                variant="raised"
                color="primary"
                className={classes.submit}
                type="submit"
              >
                Sign Up
              </Button>
              or already a user??
              <Button
                fullWidth
                variant="raised"
                color="primary"
                className={classes.submit}
                onClick={this.onUserSignIn.bind(this)}
                type="submit"
              >
                SignIn
              </Button>
            </ValidatorForm>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

Register.propTypes = {
  classes: PropTypes.object.isRequired
};
//propsları maplemeye yarar
const mapStateToProps = state => {
  return state;
};

//dispatchleri maplemeye yarar
const mapDispatchToProps = {
  onUserNameChange: userNameChange,
  onUserEmailChange: userEmailChange,
  onUserPasswordChange: userPasswordChange,
  onUserPasswordConfirmationChange: userPasswordConfirmationChange,
  onRegisterSubmit: userRegisterSubmit
};
const stylesRegister= withStyles(styles)(Register);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(stylesRegister);
