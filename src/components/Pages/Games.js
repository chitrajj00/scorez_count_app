import React, { Component } from "react";
import { connect } from "react-redux";
import { Typography } from "../../includes";

class Reports extends Component {
  render() {
    return (
      <Typography variant="display1" gutterBottom component="h2">
        Here we need to display all the games played till today with winners.
      </Typography>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reports);
